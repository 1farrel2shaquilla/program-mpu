// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

int cek = 0;

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;

//keperluan kalibrasi
#define ERROR_YAW 0.001
#define ERROR_PITCH 0.002
#define ERROR_ROLL 0.002
#define WAKTU_CEK     1000 //dalam milisekon

long int tSekarang;
long int tSebelum;
long int dT;
float yawSekarang, pitchSekarang, rollSekarang;
float yawSebelum, pitchSebelum, rollSebelum;
float dYaw, dPitch, dRoll;
double bufferDataKalibrasi[10];
char counterKalibrasi;

float dataYaw, dataPitch, dataRoll;
char dataYawKirim[4], dataPitchKirim[4], dataRollKirim[4];
char statusKalibrasi = 0;
float offsetSudutyaw;
float offsetSudutpitch;
float offsetSudutroll;

int val = 0, val1 = 0;;
bool i = false, j = false;


//MPU6050 mpu(0x69); // <-- use for AD0 high


#define OUTPUT_READABLE_YAWPITCHROLL

// uncomment "OUTPUT_READABLE_REALACCEL" if you want to see acceleration
// components with gravity removed. This acceleration reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
//#define OUTPUT_READABLE_REALACCEL

// uncomment "OUTPUT_READABLE_WORLDACCEL" if you want to see acceleration
// components with gravity removed and adjusted for the world frame of
// reference (yaw is relative to initial orientation, since no magnetometer
// is present in this case). Could be quite handy in some cases.
//#define OUTPUT_READABLE_WORLDACCEL

// uncomment "OUTPUT_TEAPOT" if you want output that matches the
// format used for the InvenSense teapot demo
//#define OUTPUT_TEAPOT



#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards
#define LED_PIN 13 // (Arduino is 13, Teensy is 11, Teensy++ is 6)
bool blinkState = false;
//int MPUOffsets[6] = {-1164, 531, 1360, -55, 22, 52};
int MPUOffsets[6] = {1084, 42, 1069, 84, -18,  61};


// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// packet structure for InvenSense teapot demo
uint8_t teapotPacket[14] = { '$', 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x00, '\r', '\n' };



// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}

// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {

  pinMode(6, OUTPUT); digitalWrite(6, HIGH); delay(100); digitalWrite(6, LOW);
  pinMode(7, OUTPUT); digitalWrite(7, HIGH); delay(100); digitalWrite(7, LOW);
  pinMode(8, OUTPUT); digitalWrite(8, HIGH); delay(100); digitalWrite(8, LOW);
  pinMode(9, OUTPUT); digitalWrite(9, HIGH); delay(100); digitalWrite(9, LOW);

  // join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
#endif

  // initialize serial communication
  // (115200 chosen because it is required for Teapot Demo output, but it's
  // really up to you depending on your project)
  Serial.begin(115200);
  while (!Serial); // wait for Leonardo enumeration, others continue immediately

  // NOTE: 8MHz or slower host processors, like the Teensy @ 3.3v or Ardunio
  // Pro Mini running at 3.3v, cannot handle this baud rate reliably due to
  // the baud timing being too misaligned with processor ticks. You must use
  // 38400 or slower in these cases, or use some kind of external separate
  // crystal solution for the UART timer.

  // initialize device
  Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);

  // verify connection
  Serial.println(F("Testing device connections..."));
  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  // wait for ready
  Serial.println(F("\nSend any character to begin DMP programming and demo: "));
  //    while (Serial.available() && Serial.read()); // empty buffer
  //    while (!Serial.available());                 // wait for data
  //    while (Serial.available() && Serial.read()); // empty buffer again

  // load and configure the DMP
  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();

  // supply your own gyro offsets here, scaled for min sensitivity
  //    mpu.setXGyroOffset(220);
  //    mpu.setYGyroOffset(76);
  //    mpu.setZGyroOffset(-85);
  //    mpu.setZAccelOffset(1788); // 1688 factory default for my test chip

  mpu.setXAccelOffset(MPUOffsets[0]);
  mpu.setYAccelOffset(MPUOffsets[1]);
  mpu.setZAccelOffset(MPUOffsets[2]);
  mpu.setXGyroOffset(MPUOffsets[3]);
  mpu.setYGyroOffset(MPUOffsets[4]);
  mpu.setZGyroOffset(MPUOffsets[5]);

  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
    // turn on the DMP, now that it's ready
    Serial.println(F("Enabling DMP..."));
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }

  // configure LED for output
  pinMode(LED_PIN, OUTPUT);
}



// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

void loop() {
  int i;

  // if programming failed, don't try to do anything
  if (!dmpReady) return;

  // wait for MPU interrupt or extra packet(s) available
  while (!mpuInterrupt && fifoCount < packetSize) {
    // other program behavior stuff here
    // .
    // .
    // .
    // if you are really paranoid you can frequently test in between other
    // stuff to see if mpuInterrupt is true, and if so, "break;" from the
    // while() loop to immediately process the MPU data
    // .
    // .
    // .
  }

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & 0x02) {
    // wait for correct available data length, should be a VERY short wait
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

    // read a packet from FIFO
    mpu.getFIFOBytes(fifoBuffer, packetSize);

    // track FIFO count here in case there is > 1 packet available
    // (this lets us immediately read more without waiting for an interrupt)
    fifoCount -= packetSize;

#ifdef OUTPUT_READABLE_YAWPITCHROLL
    // display Euler angles in degrees
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

    if (statusKalibrasi == 0) //proses kalibrasi
    {
      dataYaw   = ypr[0] * 180 / M_PI;
      //              dataPitch = ypr[1] * 180/M_PI;
      //              dataRoll  = ypr[2] * 180/M_PI;

      tSekarang = millis();
      dT = tSekarang - tSebelum;

      if (dT < WAKTU_CEK)
      {
        yawSekarang   = ypr[0];
        //                pitchSekarang = ypr[1];
        //                rollSekarang  = ypr[2];
      }

      else
      {
        dYaw = abs(yawSekarang - yawSebelum);
        yawSebelum = yawSekarang;

        //                dPitch = abs(pitchSekarang - pitchSebelum);
        //                pitchSebelum = pitchSekarang;
        //
        //                dRoll = abs(rollSekarang - rollSebelum);
        //                rollSebelum = rollSekarang;

        //               Serial.print("errorYaw : ");
        //               Serial.print(dYaw,7);
        //               Serial.print("\n");
        //               Serial.print("errorPitch : ");
        //               Serial.print(dPitch,7);
        //               Serial.print("\n");
        //               Serial.print("errorRoll : ");
        //               Serial.print(dRoll,7);
        //               Serial.print("\n");

        //    Serial.print(" yawR : ");
        //    Serial.print(ypr[0],7);
        //    Serial.print(" yaw : ");
        //    Serial.println(dataYaw,7);

        if (dYaw < ERROR_YAW)
        {
          counterKalibrasi++ ;
          digitalWrite(13, 0);
        }

        else counterKalibrasi = 0;

        if (counterKalibrasi > 6)
        {
          statusKalibrasi  = 1;
          offsetSudutyaw   = yawSekarang;
          //                    offsetSudutpitch = pitchSekarang;
          //                    offsetSudutroll  = rollSekarang;

          digitalWrite(13, 1);
          Serial.println("KALIBRASI BERES!!!!!!!!!");
        }

        tSebelum = tSekarang;
      }

    }

    else
    {
      dataYaw = (ypr[0] - offsetSudutyaw) * 180 / M_PI;

      if (dataYaw > 180) dataYaw = dataYaw - 360;
      else if (dataYaw < -180) dataYaw = dataYaw + 360;

      //              dataPitch = ypr[1] * 180/M_PI;
      //
      //              if(dataPitch>180) dataPitch = dataPitch - 360;
      //              else if(dataPitch<-180) dataPitch = dataPitch + 360;
      //
      //              dataRoll = ypr[2] * 180/M_PI;
      //
      //              if(dataRoll>180) dataRoll = dataRoll - 360;
      //              else if(dataRoll<-180) dataRoll = dataRoll + 360;

      dataYawKirim[0]   = (*(long int*)&dataYaw) & 0xFF;
      dataYawKirim[1]   = ((*(long int*)&dataYaw) >> 8) & 0xFF;
      dataYawKirim[2]   = ((*(long int*)&dataYaw) >> 16) & 0xFF;
      dataYawKirim[3]   = ((*(long int*)&dataYaw) >> 24) & 0xFF;
      dataPitchKirim[0] = (*(long int*)&dataPitch) & 0xFF;
      dataPitchKirim[1] = ((*(long int*)&dataPitch) >> 8) & 0xFF;
      dataPitchKirim[2] = ((*(long int*)&dataPitch) >> 16) & 0xFF;
      dataPitchKirim[3] = ((*(long int*)&dataPitch) >> 24) & 0xFF;
      dataRollKirim[0]  = (*(long int*)&dataRoll) & 0xFF;
      dataRollKirim[1]  = ((*(long int*)&dataRoll) >> 8) & 0xFF;
      dataRollKirim[2]  = ((*(long int*)&dataRoll) >> 16) & 0xFF;
      dataRollKirim[3]  = ((*(long int*)&dataRoll) >> 24) & 0xFF;

//            Serial.print("Yaw : ");
//            Serial.print(dataYaw);
//            Serial.print("\t");
//            Serial.print("Pitch : ");
//            Serial.print(dataPitch);
//            Serial.print("\t");
//            Serial.print("Roll : ");
//            Serial.print(dataRoll);
//            Serial.println("\t");

      Serial.write('i');
      Serial.write('t');
      Serial.write('s');
      Serial.write(dataYawKirim[0]);
      Serial.write(dataYawKirim[1]);
      Serial.write(dataYawKirim[2]);
      Serial.write(dataYawKirim[3]);
      Serial.write(':');
      Serial.write(dataPitchKirim[0]);
      Serial.write(dataPitchKirim[1]);
      Serial.write(dataPitchKirim[2]);
      Serial.write(dataPitchKirim[3]);
      Serial.write(':');
      Serial.write(dataRollKirim[0]);
      Serial.write(dataRollKirim[1]);
      Serial.write(dataRollKirim[2]);
      Serial.write(dataRollKirim[3]);
    }

    //            Serial.print("ypr\t");
    //            Serial.print(ypr[0] * 180/M_PI);
    //            Serial.print("\t");
    //            Serial.print(ypr[1] * 180/M_PI);
    //            Serial.print("\t");
    //            Serial.println(ypr[2] * 180/M_PI);

#endif


    // blink LED to indicate activity
    blinkState = !blinkState;
    //digitalWrite(LED_PIN, blinkState);
  }
}
